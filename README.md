# Java and Apache ActiveMQ

This project was created to learn about how to create a Java client and communicate with Apache ActiveMQ message broker.

## The project
- Java with maven and springboot.
- I hosted ActiveMQ broker locally on my machine. 
- The connection for producers and consumers is: tcp://localhost:61616.
- To access the the broker, you can go to http://127.0.0.1:8161.

## Version
- Apache ActiveMQ is using version 5.18.3